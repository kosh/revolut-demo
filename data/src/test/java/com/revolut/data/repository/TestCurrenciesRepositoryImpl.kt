package com.revolut.data.repository

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import com.revolut.data.model.CurrencyResponseModel
import com.revolut.data.model.mapper.CurrenciesMapper
import com.revolut.data.service.CurrenciesService
import com.revolut.domain.model.CurrencyModel
import com.revolut.domain.model.RatesModel
import com.revolut.domain.model.RequestBody
import com.revolut.domain.util.DEFAULT_BASE_CURRENCY
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import java.math.BigDecimal
import java.util.*

class TestCurrenciesRepositoryImpl {

    private val service: CurrenciesService = mock()

    private lateinit var mapper: CurrenciesMapper
    private lateinit var repoImpl: CurrenciesRepositoryImpl


    @Before fun before() {
        mapper = CurrenciesMapper()
        repoImpl = CurrenciesRepositoryImpl(service, mapper)
    }

    @Test fun `test getting currencies returns error`() {
        val error = Throwable("error")
        val requestBody = getRequestBody()
        whenever(service.getCurrencies(DEFAULT_BASE_CURRENCY)).thenReturn(Single.error(error))

        repoImpl.getCurrencies(requestBody)
            .test()
            .assertSubscribed()
            .assertNoValues()
            .assertError(error)
            .dispose()
    }

    @Test fun `test getting currencies returns success and mapper handles empty data`() {
        val response = CurrencyResponseModel()

        val requestBody = getRequestBody()
        whenever(service.getCurrencies(DEFAULT_BASE_CURRENCY)).thenReturn(Single.just(response))

        repoImpl.getCurrencies(requestBody)
            .test()
            .assertSubscribed()
            .assertValue(mapper.transform(response))
            .dispose()
    }

    @Test fun `test getting currencies returns success with actual data`() {
        val date = Date()
        val currency = DEFAULT_BASE_CURRENCY
        val map = hashMapOf(
            "EUR" to BigDecimal(100.0),
            "USD" to BigDecimal(90.0)
        )

        val response = CurrencyResponseModel(date, currency, map)
        val requestBody = getRequestBody()

        whenever(service.getCurrencies(DEFAULT_BASE_CURRENCY)).thenReturn(Single.just(response))

        repoImpl.getCurrencies(requestBody)
            .test()
            .assertSubscribed()
            .assertValue(mapper.transform(response))
            .dispose()
    }

    @Test fun `test getting converting currencies  returns success with actual data`() {
        val price = 10.0
        val date = Date()
        val currency = DEFAULT_BASE_CURRENCY
        val map = hashMapOf(
            "EUR" to BigDecimal(100.0),
            "USD" to BigDecimal(90.0)
        )

        val response = CurrencyResponseModel(date, currency, map)
        val requestBody = getRequestBody(price = price)

        whenever(service.getCurrencies(DEFAULT_BASE_CURRENCY, price)).thenReturn(Single.just(response))

        repoImpl.getCurrencies(requestBody)
            .test()
            .assertSubscribed()
            .assertValue(mapper.transform(response))
            .dispose()
    }

    private fun getRequestBody(
        base: String = DEFAULT_BASE_CURRENCY,
        price: Double? = null
    ) = RequestBody(base, price)
}