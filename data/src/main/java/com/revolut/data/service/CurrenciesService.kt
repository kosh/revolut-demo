package com.revolut.data.service

import com.revolut.data.model.CurrencyResponseModel
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface CurrenciesService {
    @GET("latest")
    fun getCurrencies(
        @Query("base") base: String,
        @Query("amount") amount: Double? = null
    ): Single<CurrencyResponseModel>
}