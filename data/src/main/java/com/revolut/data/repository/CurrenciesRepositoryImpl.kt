package com.revolut.data.repository

import com.revolut.data.model.mapper.CurrenciesMapper
import com.revolut.data.service.CurrenciesService
import com.revolut.domain.model.CurrencyModel
import com.revolut.domain.model.RequestBody
import com.revolut.domain.respository.CurrenciesRepository
import io.reactivex.Single

class CurrenciesRepositoryImpl(
    private val service: CurrenciesService, // this could be the remote implementation, since we don't have datasource, lets make it simple!
    private val mapper: CurrenciesMapper
) : CurrenciesRepository {
    override fun getCurrencies(
        requestBody: RequestBody
    ): Single<CurrencyModel> = service.getCurrencies(requestBody.base, requestBody.amount).map(mapper::transform)
}