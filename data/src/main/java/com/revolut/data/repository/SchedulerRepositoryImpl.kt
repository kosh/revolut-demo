package com.revolut.data.repository

import com.revolut.domain.respository.SchedulerRepository
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Singleton

@Singleton class SchedulerRepositoryImpl @Inject constructor() : SchedulerRepository {
    override fun uiThread(): Scheduler = AndroidSchedulers.mainThread()
    override fun ioThread(): Scheduler = Schedulers.io()
}