package com.revolut.data.di.modules

import com.revolut.data.model.mapper.CurrenciesMapper
import com.revolut.data.repository.CurrenciesRepositoryImpl
import com.revolut.data.repository.SchedulerRepositoryImpl
import com.revolut.data.service.CurrenciesService
import com.revolut.domain.respository.CurrenciesRepository
import com.revolut.domain.respository.SchedulerRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module class RepositoryModule {
    @Provides @Singleton fun provideSchedulerProvider(): SchedulerRepository = SchedulerRepositoryImpl()
    @Provides @Singleton fun provideCurrenciesRepository(
        service: CurrenciesService,
        mapper: CurrenciesMapper
    ): CurrenciesRepository = CurrenciesRepositoryImpl(service, mapper)
}