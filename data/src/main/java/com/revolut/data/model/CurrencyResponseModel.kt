package com.revolut.data.model

import com.google.gson.annotations.SerializedName
import java.math.BigDecimal
import java.util.*

data class CurrencyResponseModel(
    @SerializedName("date") val date: Date? = null,
    @SerializedName("base") val baseCurrency: String? = null,
    @SerializedName("rates") val rates: Map<String, BigDecimal>? = null
)