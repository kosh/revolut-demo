package com.revolut.data.model.mapper

import com.revolut.data.model.CurrencyResponseModel
import com.revolut.domain.model.CurrencyModel
import com.revolut.domain.model.RatesModel
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton open class CurrenciesMapper @Inject constructor() {
    fun transform(currencyModel: CurrencyResponseModel): CurrencyModel {
        val rates = currencyModel.rates?.mapNotNull { entry ->
            RatesModel(entry.key, entry.value)
        }
        return CurrencyModel(currencyModel.date, currencyModel.baseCurrency, rates)
    }
}