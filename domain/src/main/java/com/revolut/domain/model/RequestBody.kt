package com.revolut.domain.model

data class RequestBody(
    val base: String,
    val amount: Double? = null
)