package com.revolut.domain.model

import java.math.BigDecimal

data class RatesModel(
    val currency: String,
    val price: BigDecimal
)