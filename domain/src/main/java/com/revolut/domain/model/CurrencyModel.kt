package com.revolut.domain.model

import java.util.*

data class CurrencyModel(
    val date: Date? = null,
    val baseCurrency: String? = null,
    val rates: List<RatesModel>? = null
)