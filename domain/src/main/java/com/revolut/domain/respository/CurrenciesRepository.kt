package com.revolut.domain.respository

import com.revolut.domain.model.CurrencyModel
import com.revolut.domain.model.RequestBody
import io.reactivex.Single

interface CurrenciesRepository {
    fun getCurrencies(
        requestBody: RequestBody
    ): Single<CurrencyModel>
}