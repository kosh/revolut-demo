package com.revolut.domain.respository

import io.reactivex.Scheduler

interface SchedulerRepository {
    fun uiThread(): Scheduler
    fun ioThread(): Scheduler
}