package com.revolut.domain.util

const val DEFAULT_BASE_CURRENCY = "EUR"
const val DEFAULT_ONE_INTERVAL = 1L
const val DEFAULT_DEBOUNCE_VALUE = 400L
const val DEFAULT_DELAY = 400L