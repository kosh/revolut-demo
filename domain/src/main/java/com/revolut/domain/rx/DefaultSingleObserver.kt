package com.revolut.domain.rx

import io.reactivex.SingleObserver
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.subscribers.ResourceSubscriber
import timber.log.Timber

open class DefaultSingleObserver<T> : DisposableSingleObserver<T>() {
    override fun onSuccess(t: T) = Unit
    override fun onError(e: Throwable) = Timber.e(e)
}