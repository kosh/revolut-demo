package com.revolut.domain.rx

import io.reactivex.subscribers.ResourceSubscriber
import timber.log.Timber

open class DefaultFlowableObserver<T> : ResourceSubscriber<T>() {
    override fun onComplete() = Unit
    override fun onNext(t: T) = Unit
    override fun onError(e: Throwable) = Timber.e(e)
}