package com.revolut.domain.usecase

import io.reactivex.Single

interface BaseSingleUseCase<P, T> {
    fun buildSingle(param: P): Single<T>
}