package com.revolut.domain.usecase

import io.reactivex.Flowable

interface BaseFlowableUseCase<P, T> {
    fun buildFlowable(param: P): Flowable<T>
}