package com.revolut.domain

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import com.revolut.domain.model.CurrencyModel
import com.revolut.domain.model.RequestBody
import com.revolut.domain.respository.CurrenciesRepository
import com.revolut.domain.util.DEFAULT_BASE_CURRENCY
import io.reactivex.Single
import org.junit.Assert.assertNull
import org.junit.Test

class TestCurrenciesRepository {

    private val repo: CurrenciesRepository = mock()
    private val requestBody = RequestBody(DEFAULT_BASE_CURRENCY)

    @Test fun `test getting currencies has no implementation`() {
        assertNull(repo.getCurrencies(requestBody))
    }

    @Test fun `test getting currencies returns error`() {
        val error = Throwable("error")
        whenever(repo.getCurrencies(requestBody)).thenReturn(Single.error(error))
        repo.getCurrencies(requestBody)
            .test()
            .assertSubscribed()
            .assertNoValues()
            .assertError(error)
            .dispose()
    }

    @Test fun `test getting currencies returns success`() {
        val response = CurrencyModel()
        whenever(repo.getCurrencies(requestBody)).thenReturn(Single.just(response))
        repo.getCurrencies(requestBody)
            .test()
            .assertSubscribed()
            .assertNoErrors()
            .assertValue(response)
            .dispose()
    }
}