package com.revolut.domain

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import com.revolut.domain.respository.SchedulerRepository
import io.reactivex.schedulers.Schedulers
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Test
import io.reactivex.android.schedulers.AndroidSchedulers

class TestSchedulerRepository {

    private val repo: SchedulerRepository = mock()

    @Test fun `Test no schedulers provided`() {
        assertNull(repo.ioThread())
        assertNull(repo.uiThread())
    }

    @Test fun `Test schedulers provided`() {
        val io = Schedulers.io()
        val ui = AndroidSchedulers.mainThread() // returnDefaultValues = true

        whenever(repo.ioThread()).thenReturn(io)
        whenever(repo.uiThread()).thenReturn(ui)

        assertEquals(io, repo.ioThread())
        assertEquals(ui, repo.uiThread())
    }
}