package com.revolut.demo.module.main

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.jraska.livedata.test
import com.nhaarman.mockitokotlin2.*
import com.revolut.domain.model.CurrencyModel
import com.revolut.domain.model.RequestBody
import com.revolut.domain.respository.SchedulerRepository
import com.revolut.domain.util.DEFAULT_BASE_CURRENCY
import com.revolut.domain.util.DEFAULT_ONE_INTERVAL
import com.revolut.demo.model.DataModel
import com.revolut.demo.model.RatesModel
import com.revolut.demo.model.mapper.DataMapper
import com.revolut.demo.module.currency.CurrencyViewModel
import com.revolut.usecase.GetCurrenciesUseCase
import com.revolut.usecase.IntervalUseCase
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import io.reactivex.schedulers.TestScheduler
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import java.math.BigDecimal
import java.util.*
import java.util.concurrent.TimeUnit

class TestCurrencyViewModel {

    @get:Rule var rule: TestRule = InstantTaskExecutorRule()

    private val schedulerProvider: SchedulerRepository = mock()
    private val getCurrenciesUseCase: GetCurrenciesUseCase = mock()
    private val intervalUseCase: IntervalUseCase = mock()
    private val context: Context = mock()

    private lateinit var mapper: DataMapper
    private lateinit var viewModel: CurrencyViewModel

    @Before fun setup() {
        whenever(schedulerProvider.ioThread()).thenReturn(Schedulers.trampoline())
        whenever(schedulerProvider.uiThread()).thenReturn(Schedulers.trampoline())
        mapper = DataMapper(context)
        viewModel = CurrencyViewModel(mapper, getCurrenciesUseCase, intervalUseCase, schedulerProvider)
    }

    @Test fun `test getting currency with default params`() {
        val response = CurrencyModel()

        val testScheduler = TestScheduler()

        whenever(intervalUseCase.buildFlowable(DEFAULT_ONE_INTERVAL)).thenReturn(
            Flowable.interval(
                1L,
                TimeUnit.SECONDS,
                testScheduler
            )
        )

        whenever(getCurrenciesUseCase.buildSingle(getRequestBody())).thenReturn(
            Single.just(response)
        )

        viewModel.getCurrency()

        testScheduler.advanceTimeTo(0, TimeUnit.SECONDS)

        viewModel.getData()
            .test()
            .assertHasValue()
            .assertValue(DataModel.Loading)

        testScheduler.advanceTimeTo(1, TimeUnit.SECONDS)

        viewModel.getData()
            .test()
            .assertHasValue()
            .assertValue(DataModel.CurrencyModel())
    }

    @Test fun `test on text changed`() {
        val currency = Currency.getInstance(DEFAULT_BASE_CURRENCY)
        val price = 60.0
        val response = CurrencyModel(
            Date(), DEFAULT_BASE_CURRENCY, listOf(
                com.revolut.domain.model.RatesModel("USD", BigDecimal(1.0)),
                com.revolut.domain.model.RatesModel("MYR", BigDecimal(5.0)),
                com.revolut.domain.model.RatesModel("EUR", BigDecimal(5))
            )
        )
        val rateModel = RatesModel(currency, BigDecimal(price), 0)

        val testScheduler = TestScheduler()

        whenever(intervalUseCase.buildFlowable(DEFAULT_ONE_INTERVAL)).thenReturn(
            Flowable.interval(
                1L,
                TimeUnit.SECONDS,
                testScheduler
            )
        )

        whenever(getCurrenciesUseCase.buildSingle(getRequestBody())).thenReturn(
            Single.just(response)
        )

        viewModel.getCurrency() // add data first to the livedata

        testScheduler.advanceTimeTo(0, TimeUnit.SECONDS)

        viewModel.getData()
            .test()
            .assertHasValue()
            .assertValue(DataModel.Loading)

        testScheduler.advanceTimeTo(1, TimeUnit.SECONDS)

        viewModel.getData()
            .test()
            .assertHasValue()
            .assertValue(mapper.map(response, null))

        whenever(getCurrenciesUseCase.buildSingle(getRequestBody(price = price))).thenReturn(
            Single.just(response)
        ) // modify the usecase now to use the price after data is set to the livedata.

        viewModel.onTextChanged(rateModel, "$price")

        testScheduler.advanceTimeTo(2, TimeUnit.SECONDS)

        viewModel.getData()
            .test()
            .assertHasValue()

        val mapped = viewModel.getData().value

        assertTrue(mapped is DataModel.CurrencyModel)

        val result = mapped as DataModel.CurrencyModel

        assertEquals(result.rates?.first(), rateModel)
        assertEquals(result, mapper.map(response, rateModel))
    }

    @Test fun `test on text changed but resetting all values to zero`() {
        val response = CurrencyModel(
            Date(), DEFAULT_BASE_CURRENCY, listOf(
                com.revolut.domain.model.RatesModel("USD", BigDecimal(1.0)),
                com.revolut.domain.model.RatesModel("MYR", BigDecimal(5.0))
            )
        )
        val price = 0.0
        val rateModel = RatesModel(Currency.getInstance(Locale.getDefault()), BigDecimal(price), 0)

        val testScheduler = TestScheduler()

        whenever(intervalUseCase.buildFlowable(DEFAULT_ONE_INTERVAL)).thenReturn(
            Flowable.interval(
                1L,
                TimeUnit.SECONDS,
                testScheduler
            )
        )

        whenever(getCurrenciesUseCase.buildSingle(getRequestBody())).thenReturn(
            Single.just(response)
        )

        viewModel.getCurrency() // add data first to the livedata

        testScheduler.advanceTimeTo(0, TimeUnit.SECONDS)

        viewModel.getData()
            .test()
            .assertHasValue()
            .assertValue(DataModel.Loading)

        testScheduler.advanceTimeTo(1, TimeUnit.SECONDS)

        viewModel.getData()
            .test()
            .assertHasValue()
            .assertValue(mapper.map(response, null))

        viewModel.onTextChanged(rateModel, "$price")

        testScheduler.advanceTimeTo(2, TimeUnit.SECONDS)

        viewModel.getData()
            .test()
            .assertHasValue()

        val mapped = viewModel.getData().value
        assertTrue(mapped is DataModel.CurrencyModel)

        val result = mapped as DataModel.CurrencyModel

        assertEquals(true, result.rates?.all { it.price.toDouble() == 0.0 })
    }

    @Test fun `test rate clicked but nothing happens due to price is zero`() {
        val response = CurrencyModel()
        val rateModel = RatesModel(Currency.getInstance(Locale.getDefault()), BigDecimal(0.0), 0)

        val testScheduler = TestScheduler()

        whenever(intervalUseCase.buildFlowable(DEFAULT_ONE_INTERVAL)).thenReturn(
            Flowable.interval(
                1L,
                TimeUnit.SECONDS,
                testScheduler
            )
        )

        whenever(getCurrenciesUseCase.buildSingle(getRequestBody())).thenReturn(
            Single.just(response)
        )

        viewModel.getCurrency()

        testScheduler.advanceTimeTo(0, TimeUnit.SECONDS)

        viewModel.getData()
            .test()
            .assertHasValue()
            .assertValue(DataModel.Loading)

        testScheduler.advanceTimeTo(1, TimeUnit.SECONDS)

        viewModel.getData()
            .test()
            .assertHasValue()
            .assertValue(DataModel.CurrencyModel())

        viewModel.onRateClicked(rateModel)

        testScheduler.advanceTimeTo(1, TimeUnit.SECONDS)

        verify(spy(viewModel), never()).getCurrency()
    }

    @Test fun `test rate clicked`() {
        val currency = Currency.getInstance(DEFAULT_BASE_CURRENCY)
        val price = 60.0
        val response = CurrencyModel(
            Date(), DEFAULT_BASE_CURRENCY, listOf(
                com.revolut.domain.model.RatesModel("USD", BigDecimal(1.0)),
                com.revolut.domain.model.RatesModel("MYR", BigDecimal(5.0)),
                com.revolut.domain.model.RatesModel("EUR", BigDecimal(5))
            )
        )
        val rateModel = RatesModel(currency, BigDecimal(price), 0)

        val testScheduler = TestScheduler()

        whenever(intervalUseCase.buildFlowable(DEFAULT_ONE_INTERVAL)).thenReturn(
            Flowable.interval(
                1L,
                TimeUnit.SECONDS,
                testScheduler
            )
        )

        whenever(getCurrenciesUseCase.buildSingle(getRequestBody())).thenReturn(
            Single.just(response)
        )

        viewModel.getCurrency() // add data first to the livedata

        testScheduler.advanceTimeTo(0, TimeUnit.SECONDS)

        viewModel.getData()
            .test()
            .assertHasValue()
            .assertValue(DataModel.Loading)

        testScheduler.advanceTimeTo(1, TimeUnit.SECONDS)

        viewModel.getData()
            .test()
            .assertHasValue()
            .assertValue(mapper.map(response, null))

        whenever(getCurrenciesUseCase.buildSingle(getRequestBody(price = price))).thenReturn(
            Single.just(response)
        ) // modify the usecase now to use the price after data is set to the livedata.

        viewModel.onRateClicked(rateModel)

        testScheduler.advanceTimeTo(2, TimeUnit.SECONDS)

        viewModel.getData()
            .test()
            .assertHasValue()

        val mapped = viewModel.getData().value

        assertTrue(mapped is DataModel.CurrencyModel)

        val result = mapped as DataModel.CurrencyModel

        assertEquals(result.rates?.first(), rateModel)
        assertEquals(result, mapper.map(response, rateModel))
    }

    private fun getRequestBody(
        base: String = DEFAULT_BASE_CURRENCY,
        price: Double? = null
    ) = RequestBody(base, price)
}