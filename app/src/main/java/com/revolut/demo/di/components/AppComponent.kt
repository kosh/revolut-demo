package com.revolut.demo.di.components

import android.app.Application
import com.revolut.data.di.modules.NetworkModule
import com.revolut.data.di.modules.RepositoryModule
import com.revolut.demo.App
import com.revolut.demo.di.modules.ActivityBindingModule
import com.revolut.demo.di.modules.ApplicationModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        ApplicationModule::class,
        NetworkModule::class,
        RepositoryModule::class,
        ActivityBindingModule::class
    ]
)
interface AppComponent : AndroidInjector<DaggerApplication> {

    @Component.Builder
    interface Builder {
        @BindsInstance fun application(application: Application): Builder
        @BindsInstance fun networkModule(module: NetworkModule): Builder
        @BindsInstance fun repoModule(module: RepositoryModule): Builder

        fun build(): AppComponent
    }

    companion object {
        fun getComponent(app: App): AppComponent = DaggerAppComponent.builder()
            .application(app)
            .networkModule(NetworkModule())
            .repoModule(RepositoryModule())
            .build()
    }
}