package com.revolut.demo.di.modules

import android.app.Application
import android.content.Context
import com.revolut.demo.di.annotations.ForApplication
import dagger.Module
import dagger.Provides

@Module(includes = [ViewModelModule::class])
class ApplicationModule {
    @Provides @ForApplication fun providesApplicationContext(application: Application): Context = application
}