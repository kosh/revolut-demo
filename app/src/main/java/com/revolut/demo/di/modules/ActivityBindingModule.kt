package com.revolut.demo.di.modules

import com.revolut.demo.di.scopes.PerActivity
import com.revolut.demo.module.main.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.android.support.AndroidSupportInjectionModule

@Suppress("unused")
@Module(
    includes = [
        AndroidSupportInjectionModule::class,
        FragmentBindingModule::class]
)
abstract class ActivityBindingModule {
    @PerActivity @ContributesAndroidInjector abstract fun provideMainActivity(): MainActivity
}