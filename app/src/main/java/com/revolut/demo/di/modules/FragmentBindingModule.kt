package com.revolut.demo.di.modules

import com.revolut.demo.di.scopes.PerFragment
import com.revolut.demo.module.currency.CurrencyFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class FragmentBindingModule {
    @PerFragment @ContributesAndroidInjector abstract fun provideCurrencyFragment(): CurrencyFragment
}