package com.revolut.demo.platform.timber

import timber.log.Timber

class AppTree : Timber.DebugTree() {
    override fun createStackElementTag(element: StackTraceElement): String? {
        val className = super.createStackElementTag(element)?.split("$")?.get(0)
        return "($className.kt:${element.lineNumber})#${element.methodName}"
    }

    companion object {
        fun initTimber() = Timber.plant(AppTree())
    }
}