package com.revolut.demo.platform.extensions

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer


/**
 * A helper method that only emit values that aren't null
 */
fun <T> LiveData<T>.observeNotNull(lifecycleOwner: LifecycleOwner, callback: (T) -> Unit) {
    this.observe(lifecycleOwner, Observer { it?.let(callback) })
}