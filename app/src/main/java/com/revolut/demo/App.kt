package com.revolut.demo

import com.revolut.demo.di.components.AppComponent
import com.revolut.demo.platform.timber.AppTree
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication

class App : DaggerApplication() {

    private val appComponent by lazy { AppComponent.getComponent(this) }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> = appComponent

    override fun onCreate() {
        super.onCreate()
        AppTree.initTimber()
    }
}