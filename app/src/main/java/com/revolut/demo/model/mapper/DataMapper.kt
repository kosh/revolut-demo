package com.revolut.demo.model.mapper

import android.annotation.SuppressLint
import android.content.Context
import com.revolut.domain.model.CurrencyModel
import com.revolut.demo.R
import com.revolut.demo.di.annotations.ForApplication
import com.revolut.demo.model.DataModel
import com.revolut.demo.model.RatesModel
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton open class DataMapper @Inject constructor(
    @ForApplication private val context: Context
) {
    fun map(
        currencyModel: CurrencyModel,
        baseRate: RatesModel?
    ): DataModel {
        val rates = currencyModel.rates
            ?.filter { it.currency != baseRate?.currencyCode && it.currency != currencyModel.baseCurrency }
            ?.map { entry ->
                val drawableResId = kotlin.runCatching {
                    if (entry.currency.equals("TRY", true)) {
                        R.drawable.flag_try
                    } else {
                        val drawableName = "flag_${entry.currency.toLowerCase(Locale.getDefault())}"
                        val resId = context.resources.getIdentifier("drawable/$drawableName", null, context.packageName)
                        return@runCatching if (resId == 0) {
                            R.drawable.ic_unknown_currency
                        } else {
                            resId
                        }
                    }
                }.getOrDefault(R.drawable.ic_unknown_currency)

                RatesModel(Currency.getInstance(entry.currency), entry.price, drawableResId)

            }?.toMutableList()

        if (rates != null && baseRate != null) {
            rates.add(0, baseRate)
        }

        return DataModel.CurrencyModel(currencyModel.date, currencyModel.baseCurrency, rates?.toList())
    }
}