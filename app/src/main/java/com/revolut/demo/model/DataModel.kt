package com.revolut.demo.model

import java.util.*


/**
 * This class can be generic and could be changed to DataModel<T> but in this example, we only have one representing class [CurrencyModel]
 */
sealed class DataModel {
    data class CurrencyModel(
        val date: Date? = null,
        val baseCurrency: String? = null,
        val rates: List<RatesModel>? = null
    ) : DataModel()

    data class Error(val message: String?) : DataModel()

    object Loading : DataModel()
}