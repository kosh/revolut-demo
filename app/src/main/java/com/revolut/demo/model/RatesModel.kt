package com.revolut.demo.model

import androidx.annotation.DrawableRes
import java.math.BigDecimal
import java.util.*

data class RatesModel(
    val currency: Currency,
    val price: BigDecimal,
    @DrawableRes val flag: Int
) {
    val currencyCode: String = currency.currencyCode
}