package com.revolut.demo.widget

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.revolut.demo.R
import com.revolut.demo.module.adapter.RatesViewHolder
import kotlinx.android.synthetic.main.currencies_layout.view.*
import kotlinx.android.synthetic.main.empty_state_layout.view.*
import kotlinx.android.synthetic.main.loading_progress_layout.view.*
import kotlinx.android.synthetic.main.rate_row_item.view.*

class CurrenciesLayout @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    override fun onFinishInflate() {
        super.onFinishInflate()
        View.inflate(context, R.layout.currencies_layout, this)
    }

    fun showProgress() {
        stopLottieAnimation()
        emptyLayout.isVisible = false
        ratesRecyclerView.isVisible = false
        progressBar.isVisible = true
    }

    fun hideProgress(showEmptyState: Boolean = false) {
        progressBar.isVisible = false
        if (showEmptyState) {
            showEmptyState()
        } else {
            hideEmptyState()
            ratesRecyclerView.isVisible = true
        }
    }

    fun setOnReloadClickListener(onClick: () -> Unit) {
        reloadBtn.setOnClickListener { onClick.invoke() }
    }

    fun setAdapter(adapter: RecyclerView.Adapter<out RecyclerView.ViewHolder>) {
        /* we are updating the recyclerview instantly, so avoid using animation that feels flickering!
        or better cache the drawables and then we could remove this.
        */
        ratesRecyclerView.itemAnimator = null
        ratesRecyclerView.adapter = adapter
    }

    fun scrollRecyclerViewToTopAndMakeItemFirstResponder() {
        ratesRecyclerView.scrollToPosition(0)
        val viewHolder = ratesRecyclerView.findViewHolderForAdapterPosition(0) as? RatesViewHolder
        viewHolder?.itemView?.editText?.requestFocus()
    }

    private fun showEmptyState() {
        ratesRecyclerView.isVisible = false
        emptyLayout.isVisible = true
        playLottieAnimation()
    }

    private fun hideEmptyState() {
        stopLottieAnimation()
        emptyLayout.isVisible = false
    }

    private fun stopLottieAnimation() {
        if (lottieImage.isAnimating) {
            lottieImage.cancelAnimation()
        }
    }

    private fun playLottieAnimation() {
        if (!lottieImage.isAnimating) {
            lottieImage.playAnimation()
        }
    }
}