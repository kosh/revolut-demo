package com.revolut.demo.module.currency

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.revolut.domain.respository.SchedulerRepository
import com.revolut.demo.R
import com.revolut.demo.core.BaseFragment
import com.revolut.demo.model.DataModel
import com.revolut.demo.module.adapter.RatesAdapter
import com.revolut.demo.platform.extensions.observeNotNull
import gun0912.tedkeyboardobserver.TedRxKeyboardObserver
import kotlinx.android.synthetic.main.currency_fragment_layout.*
import timber.log.Timber
import javax.inject.Inject

class CurrencyFragment : BaseFragment() {

    override val layoutResId: Int
        get() = R.layout.currency_fragment_layout

    @Inject lateinit var viewModelFactory: ViewModelProvider.Factory
    @Inject lateinit var schedulerRepository: SchedulerRepository

    private val viewModel by lazy { ViewModelProviders.of(this, viewModelFactory).get(CurrencyViewModel::class.java) }
    private val adapter by lazy {
        RatesAdapter({ rate ->
            viewModel.onRateClicked(rate)
        }, { ratesModel, value ->
            viewModel.onTextChanged(ratesModel, value)
        }, {
            viewModel.onPauseResumeRetrievingCurrencies(it)
        })
    }
    private val adapterObserver by lazy {
        object : RecyclerView.AdapterDataObserver() {
            override fun onItemRangeMoved(fromPosition: Int, toPosition: Int, itemCount: Int) {
                super.onItemRangeMoved(fromPosition, toPosition, itemCount)
                if (toPosition == 0) { // only if an item clicked and became first responder.
                    currenciesLayout.scrollRecyclerViewToTopAndMakeItemFirstResponder()
                }
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        currenciesLayout.setOnReloadClickListener { viewModel.getCurrency() }
        adapter.registerAdapterDataObserver(adapterObserver)
        currenciesLayout.setAdapter(adapter)
        observeChanges()

        if (savedInstanceState == null) { // load only on first time creation
            viewModel.getCurrency()
        }

        add(
            TedRxKeyboardObserver(requireActivity()).listen()
                .subscribe({
                    if (!it) {
                        currenciesLayout.requestFocus() // remove the cursor from the edittext when keyboard is invisible
                    }
                    viewModel.onPauseResumeRetrievingCurrencies(it)
                }, {
                    Timber.e(it)
                })
        )
    }

    override fun onDestroyView() {
        adapter.unregisterAdapterDataObserver(adapterObserver)
        super.onDestroyView()
    }

    private fun observeChanges() {
        viewModel.getData().observeNotNull(this) {
            when (it) {
                is DataModel.Loading -> currenciesLayout.showProgress()
                is DataModel.Error -> {
                    currenciesLayout.hideProgress(true)
                    showSnackBar(currenciesLayout, it.message)
                }
                is DataModel.CurrencyModel -> {
                    currenciesLayout.hideProgress(it.rates.isNullOrEmpty())
                    adapter.submitList(it.rates)
                }
            }
        }
    }
}