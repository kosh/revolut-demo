package com.revolut.demo.module.currency

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.revolut.domain.model.RequestBody
import com.revolut.domain.respository.SchedulerRepository
import com.revolut.demo.core.BaseViewModel
import com.revolut.demo.model.DataModel
import com.revolut.demo.model.RatesModel
import com.revolut.demo.model.mapper.DataMapper
import com.revolut.domain.util.DEFAULT_BASE_CURRENCY
import com.revolut.domain.util.DEFAULT_ONE_INTERVAL
import com.revolut.usecase.GetCurrenciesUseCase
import com.revolut.usecase.IntervalUseCase
import hu.akarnokd.rxjava2.operators.FlowableTransformers
import io.reactivex.Single
import io.reactivex.processors.PublishProcessor
import java.math.BigDecimal
import javax.inject.Inject

class CurrencyViewModel @Inject constructor(
    private val mapper: DataMapper,
    private val getCurrenciesUseCase: GetCurrenciesUseCase,
    private val intervalUseCase: IntervalUseCase,
    schedulerRepository: SchedulerRepository
) : BaseViewModel(schedulerRepository) {

    private val dataLiveData = MutableLiveData<DataModel>()
    private var baseRate: RatesModel? = null
    private val publishProcessorValve = PublishProcessor.create<Boolean>() // acts a valve to pause/resume the api request.

    fun getData(): LiveData<DataModel> = dataLiveData

    fun onPauseResumeRetrievingCurrencies(isKeyboardVisible: Boolean) {
        publishProcessorValve.onNext(!isKeyboardVisible)
    }

    fun getCurrency(
        base: String = DEFAULT_BASE_CURRENCY,
        amount: Double? = null
    ) {
        safeSubscribe(intervalUseCase.buildFlowable(DEFAULT_ONE_INTERVAL)
            .compose(FlowableTransformers.valve(publishProcessorValve)) // this could be avoided by using disposeAll() and resubscribing
            .flatMapSingle { getCurrenciesUseCase.buildSingle(RequestBody(base, amount)) }
            .doOnSubscribe { if (base == DEFAULT_BASE_CURRENCY && baseRate == null) dataLiveData.postValue(DataModel.Loading) }
            .map { mapper.map(it, baseRate) }
            .doOnNext { dataLiveData.postValue(it) }
            .doOnError {
                dataLiveData.postValue(DataModel.Error(it.message)) // for now only one error type.
            })
    }

    fun onTextChanged(rate: RatesModel, value: String?) {
        val price = value?.toDoubleOrNull() ?: 0.0
        disposeAll() // cancel existing observer and lets start new one to avoid having multiple observers running.
        if (value.isNullOrEmpty() || price == 0.0) {
            safeSubscribe(Single.just(dataLiveData.value as DataModel.CurrencyModel)
                .map { currencyModel ->
                    val rates = currencyModel.rates?.map { RatesModel(it.currency, BigDecimal(0.0), it.flag) }
                    DataModel.CurrencyModel(currencyModel.date, currencyModel.baseCurrency, rates)
                }
                .doOnSuccess { dataLiveData.postValue(it) })
            return
        }
        baseRate = RatesModel(rate.currency, BigDecimal(price), rate.flag)
        getCurrency(rate.currencyCode, price)
    }

    fun onRateClicked(rate: RatesModel) {
        if (baseRate != rate && rate.price.toDouble() != 0.0) {
            baseRate = rate
            disposeAll() // cancel existing observer and lets start new one to avoid having multiple observers running.
            getCurrency(rate.currencyCode)
        }
    }
}