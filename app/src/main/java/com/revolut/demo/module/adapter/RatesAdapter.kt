package com.revolut.demo.module.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.jakewharton.rxbinding3.view.clicks
import com.jakewharton.rxbinding3.view.detaches
import com.jakewharton.rxbinding3.view.focusChanges
import com.jakewharton.rxbinding3.widget.afterTextChangeEvents
import com.revolut.domain.respository.SchedulerRepository
import com.revolut.demo.model.RatesModel
import com.revolut.domain.util.DEFAULT_DEBOUNCE_VALUE
import com.revolut.domain.util.DEFAULT_DELAY
import io.reactivex.Observable
import kotlinx.android.synthetic.main.rate_row_item.view.*
import timber.log.Timber
import java.util.*
import java.util.concurrent.TimeUnit

class RatesAdapter(
    private val onClick: (RatesModel) -> Unit,
    private val onAfterTextChanged: (RatesModel, String?) -> Unit,
    private val onFocused: (Boolean) -> Unit
) : ListAdapter<RatesModel, RatesViewHolder>(itemCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = RatesViewHolder(parent, Locale.getDefault()).apply {
        itemView.clicks()
            .takeUntil(parent.detaches())
            .flatMap { getItem(adapterPosition)?.let { Observable.just(it) } ?: Observable.empty() }
            .subscribe(onClick)

        itemView.editText.apply {
            focusChanges()
                .takeUntil(parent.detaches())
                .distinctUntilChanged()
                .subscribe(onFocused)

            afterTextChangeEvents()
                .takeUntil(parent.detaches())
                .skip(1)
                .debounce(DEFAULT_DEBOUNCE_VALUE, TimeUnit.MILLISECONDS)
                .distinctUntilChanged()
                .filter { isFocused && it.editable?.toString()?.toBigDecimalOrNull() != getItem(adapterPosition)?.price }
                .delay(DEFAULT_DELAY, TimeUnit.MILLISECONDS)
                .subscribe({ t ->
                    getItem(adapterPosition)?.let { onAfterTextChanged(it, t.editable?.toString()) }
                }, {
                    Timber.e(it)
                })
        }
    }

    override fun onBindViewHolder(holder: RatesViewHolder, position: Int) = holder.bind(getItem(position))

    companion object {
        private fun itemCallback(): DiffUtil.ItemCallback<RatesModel?> {
            return object : DiffUtil.ItemCallback<RatesModel?>() {
                override fun areItemsTheSame(oldItem: RatesModel, newItem: RatesModel): Boolean = oldItem.currencyCode == newItem.currencyCode
                override fun areContentsTheSame(oldItem: RatesModel, newItem: RatesModel): Boolean = oldItem == newItem
            }
        }
    }
}