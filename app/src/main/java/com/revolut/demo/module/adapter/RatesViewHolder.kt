package com.revolut.demo.module.adapter

import android.view.ViewGroup
import com.revolut.demo.R
import com.revolut.demo.core.BaseViewHolder
import com.revolut.demo.model.RatesModel
import kotlinx.android.synthetic.main.rate_row_item.view.*
import java.util.*

class RatesViewHolder(
    parent: ViewGroup,
    private val locale: Locale
) : BaseViewHolder<RatesModel>(R.layout.rate_row_item, parent) {
    override fun bind(item: RatesModel) {
        itemView.apply {
            title.text = item.currencyCode
            val displayName = item.currency.getDisplayName(locale)
            description.text = displayName
            flag.setImageResource(item.flag)
            flag.contentDescription = displayName
            editText.setText("${item.price.toDouble()}")
            editText.setSelection(editText.text?.length ?: 0) // always set the cursor at the end of text
        }
    }
}