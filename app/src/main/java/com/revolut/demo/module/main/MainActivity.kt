package com.revolut.demo.module.main

import android.os.Bundle
import androidx.fragment.app.commit
import com.revolut.demo.R
import com.revolut.demo.core.BaseActivity
import com.revolut.demo.module.currency.CurrencyFragment

class MainActivity : BaseActivity() {
    override val layoutResId: Int
        get() = R.layout.activity_layout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            supportFragmentManager.commit {
                replace(R.id.container, CurrencyFragment())
            }
        }
    }
}