package com.revolut.demo.core

import androidx.lifecycle.ViewModel
import com.revolut.domain.respository.SchedulerRepository
import com.revolut.domain.rx.DefaultFlowableObserver
import com.revolut.domain.rx.DefaultSingleObserver
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BaseViewModel(
    private val schedulerRepository: SchedulerRepository
) : ViewModel() {

    private val disposable = CompositeDisposable()

    override fun onCleared() = disposeAll()

    protected fun <T> safeSubscribe(flowable: Flowable<T>) = add(
        flowable.subscribeOn(schedulerRepository.ioThread())
            .observeOn(schedulerRepository.uiThread())
            .subscribeWith(DefaultFlowableObserver<T>())
    )

    protected fun <T> safeSubscribe(single: Single<T>) = add(
        single.subscribeOn(schedulerRepository.ioThread())
            .observeOn(schedulerRepository.uiThread())
            .subscribeWith(DefaultSingleObserver<T>())
    )

    protected fun disposeAll() = disposable.clear()
    private fun add(disposable: Disposable) = this.disposable.add(disposable) // private for the sake of this test usage
}