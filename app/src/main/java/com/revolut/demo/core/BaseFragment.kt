package com.revolut.demo.core

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.LayoutRes
import com.google.android.material.snackbar.Snackbar
import com.revolut.demo.R
import dagger.android.support.DaggerFragment
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BaseFragment : DaggerFragment() {

    @get:LayoutRes abstract val layoutResId: Int
    private val disposable = CompositeDisposable()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater.inflate(layoutResId, container, false)
    }

    override fun onDestroyView() {
        disposable.clear()
        super.onDestroyView()
    }

    protected fun add(disposable: Disposable) = this.disposable.add(disposable)

    protected fun showSnackBar(
        view: View?,
        message: String? = null
    ) {
        val errorMsg = message ?: getString(R.string.general_error)
        view?.let { Snackbar.make(it, errorMsg, Snackbar.LENGTH_LONG).show() } ?: run {
            Toast.makeText(requireContext().applicationContext, errorMsg, Toast.LENGTH_LONG).show() // fallback
        }
    }
}