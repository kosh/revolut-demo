package com.revolut.usecase

import com.revolut.domain.model.CurrencyModel
import com.revolut.domain.model.RequestBody
import com.revolut.domain.respository.CurrenciesRepository
import com.revolut.domain.respository.SchedulerRepository
import com.revolut.domain.usecase.BaseSingleUseCase
import io.reactivex.Single
import javax.inject.Inject

class GetCurrenciesUseCase @Inject constructor(
    private val currenciesRepository: CurrenciesRepository,
    private val schedulerRepository: SchedulerRepository
) : BaseSingleUseCase<RequestBody, CurrencyModel> {
    override fun buildSingle(param: RequestBody): Single<CurrencyModel> = currenciesRepository.getCurrencies(param)
        .subscribeOn(schedulerRepository.ioThread())
        .observeOn(schedulerRepository.uiThread())
}