package com.revolut.usecase

import com.revolut.domain.respository.SchedulerRepository
import com.revolut.domain.usecase.BaseFlowableUseCase
import io.reactivex.Flowable
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class IntervalUseCase @Inject constructor(
    private val schedulerRepository: SchedulerRepository
) : BaseFlowableUseCase<Long, Long> {
    override fun buildFlowable(param: Long): Flowable<Long> = Flowable.interval(param, TimeUnit.SECONDS, schedulerRepository.ioThread())
}