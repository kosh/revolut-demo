package com.revolut.usecase

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import com.revolut.domain.respository.SchedulerRepository
import com.revolut.domain.util.DEFAULT_DEBOUNCE_VALUE
import com.revolut.domain.util.DEFAULT_ONE_INTERVAL
import io.reactivex.schedulers.TestScheduler
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import java.util.concurrent.TimeUnit

class TestIntervalUseCase {

    private lateinit var usecase: IntervalUseCase
    private val schedulerRepository: SchedulerRepository = mock()
    private val testScheduler = TestScheduler()

    @Before fun before() {
        whenever(schedulerRepository.ioThread()).thenReturn(testScheduler)
        usecase = IntervalUseCase(schedulerRepository)
    }

    @Test fun `test building flowable`() {
        assertNotNull(usecase.buildFlowable(DEFAULT_DEBOUNCE_VALUE))
    }

    @Test fun `test flowable subscription`() {

        val testObserver = usecase.buildFlowable(DEFAULT_ONE_INTERVAL).test()

        testObserver
            .assertSubscribed()
            .assertNoValues()

        testScheduler.advanceTimeBy(1L, TimeUnit.SECONDS);
        testObserver.assertValueCount(1)
        testScheduler.advanceTimeBy(4L, TimeUnit.SECONDS);
        testObserver.assertValueCount(5)
            .assertValues(0, 1, 2, 3, 4)
            .dispose()

        assertEquals(true, testObserver.isDisposed)
    }

}