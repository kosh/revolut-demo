package com.revolut.usecase

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import com.revolut.domain.model.CurrencyModel
import com.revolut.domain.model.RequestBody
import com.revolut.domain.respository.CurrenciesRepository
import com.revolut.domain.respository.SchedulerRepository
import com.revolut.domain.util.DEFAULT_BASE_CURRENCY
import io.reactivex.Single
import io.reactivex.schedulers.TestScheduler
import org.junit.Before
import org.junit.Test

class TestGetCurrenciesUseCase {

    private lateinit var usecase: GetCurrenciesUseCase
    private val repo: CurrenciesRepository = mock()
    private val schedulerRepository: SchedulerRepository = mock()
    private val testScheduler = TestScheduler()
    private val requestBody = RequestBody(DEFAULT_BASE_CURRENCY)

    @Before fun before() {
        whenever(schedulerRepository.ioThread()).thenReturn(testScheduler)
        whenever(schedulerRepository.uiThread()).thenReturn(testScheduler)
        usecase = GetCurrenciesUseCase(repo, schedulerRepository)
    }

    @Test fun `test getting currencies returns error`() {
        val error = Throwable("error")
        whenever(repo.getCurrencies(requestBody)).thenReturn(Single.error(error))
        val testObserver = usecase.buildSingle(requestBody)
            .test()

        testScheduler.triggerActions()

        testObserver.assertSubscribed()
            .assertNoValues()
            .assertError(error)
            .dispose()
    }

    @Test fun `test getting currencies returns success`() {
        val response = CurrencyModel()
        whenever(repo.getCurrencies(requestBody)).thenReturn(Single.just(response))
        val testObserver = usecase.buildSingle(requestBody)
            .test()

        testScheduler.triggerActions()

        testObserver.assertSubscribed()
            .assertNoErrors()
            .assertValue(response)
            .dispose()
    }
}